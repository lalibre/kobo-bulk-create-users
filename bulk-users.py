import pandas as pd
# we need to let Django handle the password hashing
from django.contrib.auth.hashers import make_password

users = pd.read_csv('media/kobo-bulk-create-users/users.csv')
permissions = pd.read_csv('media/kobo-bulk-create-users/permissions.csv')

# let's create some users with default passwords equal to their username
for i, row in users.iterrows():
    user = User.objects.create(
        username=row['username'],
        first_name=row['first_name'],
        last_name=row['last_name'],
        email=row['email'],
        password=make_password( row['password'] )
    )
    
    # create email and confirm
    email = EmailAddress.objects.create(user_id=user.id, email=row['email'])
    email.verified = True
    email.save()


# let's assign permissions
for i, row in permissions.iterrows():
    try:
        asset = Asset.objects.get(uid=row['project_uid'])
    except Asset.DoesNotExist:
        print("Project don't found")
        continue
    try:
        user = User.objects.get(username=row['username'])
    except User.DoesNotExist:
        print("User don't found")
        continue
    asset.assign_perm(user, row['permission'])


   
