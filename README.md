Copy this repo into **/opt/kobo-docker/.vols/kpi_media/**

Update the content of files

You can get into the kpi container doing:

`docker exec -it [kpi_containter] bash`

Install pandas if not present:

`pip install pandas`

Then enter the Django shell:

`./manage.py shell_plus `

Copy the content of _bulk-users.py_ script into the shell 

Finally run curl command to make the first login via ws:

`curl -X GET https://server/api/v1/ -u 'username':'passwd'`
